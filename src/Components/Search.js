import React from "react";

const Search = ({ value, onChange, onSearch }) => {
    return (
        <div className='search-box'>
            <input 
                type='input'
                className='search-bar'
                placeholder='Search...'
                onChange={e => onChange(e.target.value)}
                value={value}
                onKeyPress={e => onSearch(e)}
                >
            </input>
        </div>
    );
};

export default Search;