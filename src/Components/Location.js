import React from 'react';
import DATE_NAMES from '../assets/dateNames';

const dateBuilder = (d) => {
    const day = DATE_NAMES.days[d.getDay()];
    const date = d.getDate();
    const month = DATE_NAMES.months[d.getMonth()];
    const year = d.getFullYear();

    return `${day} ${date} ${month} ${year}`
};

const Location = ({ city, country }) => {
  return (
    <div className="location-box">
      <p className="location-city">{city}, {country}</p>
      <p className="location-date">{dateBuilder(new Date())}</p>
    </div>
  );
};

export default Location;
