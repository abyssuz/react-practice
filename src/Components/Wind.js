import React from 'react';
import windCalc from '../assets/windCalc'

const Wind = ({ wind }) => {
  const windStrength = windCalc.speed(wind.speed);
  const windDirection = windCalc.direction(wind.deg);

  return (
    <div className='wind-box'>
      <p className='wind-direction'>Wind direction: {windDirection.name}</p>
      <img src={windDirection.icon} alt='direction-icon'></img>
      <p className='wind-speed'>Wind speed: {windStrength.speed}, {windStrength.type}</p>
    </div>
  );
};

export default Wind;