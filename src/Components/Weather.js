import React from "react";
import WEATHER_TYPE from '../assets/weatherType';
import clear from '../assets/clear.png';
import clouds from '../assets/clouds.png';
import rain from '../assets/rain.png';
import Wind from "./Wind";

const getWeatherImg = (weather) => {
    switch(weather) {
        case WEATHER_TYPE.clear: return clear;
        case WEATHER_TYPE.clouds: return clouds;
        case WEATHER_TYPE.rain: return rain;
    }
}

const WeatherInfo = ({ temp, feel, weather, wind }) => {
    return (
        <div className='weather-box'>
            <p className='weather-temp'>{Math.round(temp)}°c</p>
            <p className='weather-feel'>feels like: {Math.round(feel)}°c</p>
            <p className='weather'>{weather}</p>
            <img className='weather-img' src={getWeatherImg(weather)} alt='weather' />
            <Wind wind={wind}/>
        </div>
    );
};

export default WeatherInfo;