import React, { useState } from 'react';
import './style.css';
import Search from './Components/Search';
import Location from './Components/Location';
import Weather from './Components/Weather';
import WEATHER_API from './assets/weatherAPI';

const App = () => {
  const [query, setQuery] = useState('');
  const [weather, setWeather] = useState({});

  const onSearch = (e) => {
    if (e.key === 'Enter') {
      fetch(`${WEATHER_API.url}weather?q=${query}&units=metric&APPID=${WEATHER_API.key}`)
        .then((res) => res.json())
        .then((data) => {
          setWeather(data);
          setQuery('');
        });
    }
  };

  return (
    <div className="main-page">
      <Search value={query} onChange={setQuery} onSearch={onSearch} />
      {typeof weather.main != 'undefined' ? (
        <div>
          <Location city={weather.name} country={weather.sys.country} />
          <Weather temp={weather.main.temp} feel={weather.main.feels_like} weather={weather.weather[0].main} wind={weather.wind}/>
        </div>
      ) : ('')}
    </div>
  );
};

export default App;
