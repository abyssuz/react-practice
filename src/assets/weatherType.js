const WEATHER_TYPE = {
    clear: 'Clear',
    rain: 'Rain',
    clouds: 'Clouds',
}

export default WEATHER_TYPE;