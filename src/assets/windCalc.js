import north from './windDirectionsIcons/north.png';
import northEast from './windDirectionsIcons/north-east.png';
import northWest from './windDirectionsIcons/north-west.png';
import south from './windDirectionsIcons/south.png';
import southEast from './windDirectionsIcons/south-east.png';
import southWest from './windDirectionsIcons/south-west.png';
import west from './windDirectionsIcons/west.png';
import east from './windDirectionsIcons/east.png';

const windCalc = {
  direction: (windDeg) => {
    if (windDeg > 337 || windDeg < 22) 
      return { name: 'North', icon: north };
    else if (windDeg > 22 && windDeg < 67)
      return { name: 'North-east', icon: northEast };
    else if (windDeg > 67 && windDeg < 112) 
      return { name: 'East', icon: east };
    else if (windDeg > 112 && windDeg < 157)
      return { name: 'South-east', icon: southEast };
    else if (windDeg > 157 && windDeg < 202)
      return { name: 'South', icon: south };
    else if (windDeg > 202 && windDeg < 247)
      return { name: 'South-west', icon: southWest };
    else if (windDeg > 247 && windDeg < 292)
      return { name: 'West', icon: west };
    else if (windDeg > 292 && windDeg < 337)
      return { name: 'North-west', icon: northWest };
  },
  speed: (windSpeed) => {
    switch (Math.round(windSpeed)) {
      case 0:
        return { type: 'Calm', speed: '<2 km/h' };
      case 1:
        return { type: 'Light air', speed: '2-5 km/h' };
      case 2:
        return { type: 'Light breeze', speed: '6-11 km/h' };
      case 3:
        return { type: 'Gentle breeze', speed: '12-19 km/h' };
      case 4:
        return { type: 'Moderate breeze', speed: '20-28 km/h' };
      case 5:
        return { type: 'Fresh breeze', speed: '29–38 km/h' };
      case 6:
        return { type: 'Strong breeze', speed: '39–49 km/h' };
      case 7:
        return { type: 'High wind', speed: '50–61 km/h' };
      case 8:
        return { type: 'Fresh gale', speed: '62–74 km/h' };
      case 9:
        return { type: 'Strong gale', speed: '75–88 km/h' };
      case 10:
        return { type: 'Storm', speed: '89–102 km/h' };
      case 11:
        return { type: 'Violent storm', speed: '103–117 km/h' };
      case 12:
        return { type: 'Hurricane', speed: '>118 km/h' };
    }
  },
};

export default windCalc;
